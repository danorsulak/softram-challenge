
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const helmet = require('helmet');
var hsts = require('hsts');
const path = require('path');
var xssFilter = require('x-xss-protection');
var nosniff = require('dont-sniff-mimetype');
const request = require('request');
const axios = require('axios');
const app = express();
const {check, validationResult} = require('express-validator/check');

app.use(cors());
app.use(express.static('assets'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.disable('x-powered-by');
app.use(xssFilter());
app.use(nosniff());
app.set('etag', false);
app.use(
  helmet({
    noCache: true
  })
);
app.use(
  hsts({
    maxAge: 15552000 // 180 days in seconds
  })
);

app.use(
  express.static(path.join(__dirname, 'dist/softrams-racing'), {
    etag: false
  })
);

// Provides array of all members
app.get('/api/members', (req, res) => {
  request('http://localhost:3000/members', (err, response, body) => {
    if (response.statusCode <= 500) {
      res.send(body);
    }
  });
});

// provides specific member
app.get('/api/members/:id', (req, res) => {
    request('http://localhost:3000/members/' + req.params.id, (err, response, body) => {
        console.log('BODY IS:::');
        console.log(body);
        if (response.statusCode <= 500) {
            res.send(body);
        }
    });
});

// Provides array of team names
// TODO: Dropdown!
app.get('/api/teams', (req, res) => {
  request('http://localhost:3000/teams', (err, response, body) => {
      if (response.statusCode <= 500) {
          res.send(body);

      }
  });
});

// Submit Form!
// Saves new team member
// Validates data passed to server
app.post('/api/addMember', [
    check('firstName').not().isEmpty().withMessage('Name must have more than 5 characters'),
    check('lastName').not().isEmpty(),
    check('jobTitle').not().isEmpty(),
    check('team').not().isEmpty(),
    check('status').not().isEmpty(),
    ],
    (req, res) => {
    // res.end();
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        console.log('failed server validation');
      return res.status(422).json({ errors: errors.array() });
     }
        console.log('passed server validation');
        return axios.post('http://localhost:3000/members', req.body).then(resp => {
        console.log(resp.data);
        res.send(req.body);
    }).catch(error => {
        console.log(error);
    });
});

// Updates existing team member
// Validates data passed to server
app.put('/api/updateMember/:id', [
    check('firstName').not().isEmpty().withMessage('Name must have more than 5 characters'),
    check('lastName').not().isEmpty(),
    check('jobTitle').not().isEmpty(),
    check('team').not().isEmpty(),
    check('status').not().isEmpty(),
],
    (req, res) => {
    let id = req.body.id;
    return axios.put('http://localhost:3000/members/' + id, req.body).then(resp => {
        console.log(resp.data);
        res.send(req.body);
    }).catch(error => {
        console.log(error);
    });
});

// Removes member from member list arrray
app.delete('/api/deleteMember', function(req, res) {
    console.log('made it to server delete');
    // console.log(req.body);
    // res.end();
    let id = req.body.id;
    return axios.delete ('http://localhost:3000/members/' + id, id).then(resp => {
        // console.log(resp.data);
        res.send(req.body);
    }).catch(error => {
        console.log(error);
    });
});

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/softrams-racing/index.html'));
});

app.listen('8000', () => {
  console.log('Vrrrum Vrrrum! Server starting!');
});
