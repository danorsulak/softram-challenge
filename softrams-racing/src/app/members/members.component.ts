import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css']
})
export class MembersComponent implements OnInit {
  members = [];
  member = {};
  constructor(public appService: AppService, private router: Router) {}

  ngOnInit() {
      this.subscribeToData();
  }

  goToAddMemberForm(id) {
     // "add member" button clicked
     this.router.navigate(['/member-details/' + id ]);
  }

  editMemberByID(id: number) {
      // Edit member button clicked
    console.log('EDIT id:' + id);
      this.goToAddMemberForm(id);
  }

  deleteMemberById(id: number) {
      // Delete member button clicked, first confirm
      if (confirm('Are you sure to delete id:' + id + '?')) {
          this.appService.deleteMember(this.getMember(id)).subscribe(result => {
              if (result) {
                  // User confirmed deletion
                  console.log('API RESP GOOD!');
                  this.subscribeToData();
              }
          });
      }
  }
  getMember(id) {
      // return member object for given ID
      return this.members.filter(x => x.id === id)[0];
  }
  subscribeToData() {
      // Gets updated member data
      this.appService.getMembers().subscribe(members => (this.members = members));
  }
}
