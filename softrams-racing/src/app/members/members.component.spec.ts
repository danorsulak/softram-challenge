import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MembersComponent } from './members.component';

import {ActivatedRoute, Router} from '@angular/router';

import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import {of} from 'rxjs';
describe('MembersComponent', () => {
  let comp: MembersComponent;
  let fixture: ComponentFixture<MembersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MembersComponent],
      imports: [HttpClientModule, RouterModule],
      providers: [
        {
          provide: Router,
          useClass: class {
            navigate = jasmine.createSpy('navigate');
          }
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MembersComponent);
    comp = fixture.componentInstance;
    fixture.detectChanges();
  });
    beforeEach(() => {
        spyOn(window, 'confirm').and.returnValue(true);
    });
  afterEach(() => {
      fixture.destroy();
    });
  it('should create', () => {
    expect(comp).toBeTruthy();
  });
    it('navigate to detail page', async(() => {
        comp.editMemberByID(3);
        expect(true);
    }));
    it('delete member', async(() => {
        const highestUserNum = Math.max.apply(Math, comp.members.map(function(o) { return o.id; }));
        comp.deleteMemberById(highestUserNum);
        const newHighestUserNum = Math.max.apply(Math, comp.members.map(function(o) { return o.id; }));
        expect(highestUserNum > newHighestUserNum);
    }));
});
