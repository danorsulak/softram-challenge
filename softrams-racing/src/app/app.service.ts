import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {HttpErrorResponse} from '@angular/common/http';
import {Member} from './data-models/member-data';
import {Observable, throwError} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AppService {
    api = 'http://localhost:8000/api';
    username: string;
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    };

    constructor(private http: HttpClient) {
    }

    // Returns all members
    getMembers(): Observable<any[]> {
        return this.http
            .get<any[]>(`${this.api}/members`)
            .pipe(catchError(this.handleError.bind(this)));
    }

    getMember(id): Observable<Member> {
        return this.http
            .get<Member>(`${this.api}/members/` + id)
            .pipe(catchError(this.handleError.bind(this)));
    }

    setUsername(name: string): void {
        this.username = name;
    }

    isLoggedIn() {
        return this.username !== '';
    }

    addMember(member: Member): Observable<Member> {
        return this.http.post<Member>(`${this.api}/addMember`, member, this.httpOptions)
            .pipe(
                catchError(this.handleError.bind(this))
    );
    }

    deleteMember(member: Member): Observable<Member> {
        let httpDeleteOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            }),
            body: member
        };
        return this.http.delete<Member>(`${this.api}/deleteMember`, httpDeleteOptions)
            .pipe(
                catchError(this.handleError.bind(this))
            );
    }

    updateMember(member: Member, id: string): Observable<Member> {
        return this.http.put<Member>(`${this.api}/updateMember/` + id, member, this.httpOptions)
            .pipe(
                catchError(this.handleError.bind(this))
            );
    }

    getTeams(): Observable<any> {
        return this.http
            .get(`${this.api}/teams`)
            .pipe(catchError(this.handleError.bind(this)));
    }

    private handleError(error: HttpErrorResponse) {
        let errorMessage = 'Your request was not processed due to the following error: <br /><br />';
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
            errorMessage = errorMessage + `Error: ${error.error.message}`;

        } else {
            errorMessage = errorMessage + `Error Code: ${error.status}\nMessage: ${error.message}`;
            console.error(
                `Backend returned code ${error.status}, ` + `body was: ${error.error}`
            );
        }
       /// this.confirmationDialogService.confirm('Warning!', errorMessage, 'OK', '', true);
        if (confirm('Error Found! ' + errorMessage)) {

        }
       return []; // throwError(error);
    }
}
