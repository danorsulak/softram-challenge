import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import {MemberDetailsComponent} from './member-details.component';
import {FormBuilder} from '@angular/forms';

import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Params, Router} from '@angular/router';

import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {NgbTooltip, NgbTooltipConfig} from '@ng-bootstrap/ng-bootstrap';
import {Observable, of, Subject} from 'rxjs';
import {HttpClientTestingModule} from '../../../node_modules/@angular/common/http/testing';
import {AppService} from '../app.service';

import {Member} from '../data-models/member-data'; // DO not forget to Import
// Bonus points!
describe('MemberDetailsComponent', () => {
    let comp: MemberDetailsComponent;
    let fixture: ComponentFixture<MemberDetailsComponent>;
    let params: Subject<Params>;
    const testIds = ['', '', '', '', 7, '', 'unitTest', '', '999999999999', 'add'];
    let currentID = -1;
    var service;
    beforeEach(async(() => {
        currentID = currentID + 1;
        TestBed.configureTestingModule({
            declarations: [MemberDetailsComponent, NgbTooltip],
            imports: [
                FormsModule,
                ReactiveFormsModule,
                HttpClientModule,
                RouterModule,
                HttpClientTestingModule
            ],
            providers: [
                NgbTooltipConfig,
                HttpClient,
                FormBuilder,
                AppService,
                {
                    provide: Router,
                    useClass: class {
                        navigate = jasmine.createSpy('navigate');
                    }
                },
                {
                    provide: ActivatedRoute,
                    useValue: {
                        // params: of({id: '1'}),
                        params: of({id: testIds[currentID] }),
                    }
                }
            ]
        }).compileComponents();
        service = TestBed.get(AppService);
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MemberDetailsComponent);
        comp = fixture.componentInstance;
        fixture.detectChanges();
/*        spyOn(window, 'confirm').and.callFake(function () {
            return true;
        });*/

    });
    beforeEach(() => {
        spyOn(window, 'confirm').and.returnValue(true);
    });
    // 1 //
    it('should test member-details input validation', () => {
        expect(comp).toBeTruthy();
    });

    // 2 //
    it('should set submitted to true', fakeAsync(() => {
        comp.onSubmit();
        tick();
        expect(comp.validationPassed).toBeFalsy();
    }));

    // 3 //
    it('should trim space', fakeAsync(() => {
        comp.form.firstName = ' ';
        comp.keydown('firstName');
        tick();
        expect(comp.validationErrors.firstName === 'First Name is required');
    }));

    // 4 //
    it('SAVE- should pass validatione', fakeAsync(() => {
        // TestBed.get(ActivatedRoute).queryParams = of({ id: 'add' });
        // comp.ngOnInit();
        spyOn(service, 'addMember').and.returnValue(of(
            {
            firstName: '',
            lastName: '',
            jobTitle: '',
            team: '',
            status: ''
        }));
        comp.memberForm.controls['firstName'].setValue('Test');
        comp.memberForm.controls['lastName'].setValue('User');
        comp.memberForm.controls['jobTitle'].setValue('Unit Tester');
        comp.memberForm.controls['team'].setValue('Formula 1 - Car 77');
        comp.memberForm.controls['status'].setValue('Active');
        tick();
        // comp.id = '';
        comp.setValidationErrors();
        tick();
        comp.onSubmit();
        tick();
        expect(comp.memberForm).toBeTruthy();
    }));

    // 5 //
    it('UPDATE- valid- should pass validatione', fakeAsync(() => {
        // TestBed.get(ActivatedRoute).queryParams = of({ id: '4' });
        // comp.ngOnInit();
        spyOn(service, 'updateMember').and.returnValue(of(
            {
                firstName: '',
                lastName: '',
                jobTitle: '',
                team: '',
                status: ''
            }));
        comp.subscribeToMemberData('7');
        tick();
        comp.memberForm.controls['firstName'].setValue('UPDDATE');
        comp.memberForm.controls['lastName'].setValue('User');
        comp.memberForm.controls['jobTitle'].setValue('Unit Tester');
        comp.memberForm.controls['team'].setValue('Formula 1 - Car 77');
        comp.memberForm.controls['status'].setValue('Active');
        tick();
        comp.onSubmit();
        tick();
        expect(comp.memberForm).toBeTruthy();
    }));

    // 6 //
    it('should be invalid input', fakeAsync(() => {

        comp.memberForm.controls['firstName'].setValue('');
        tick();
        expect(comp.memberForm.valid).toBeFalsy();
    }));

    // 7 //
        it('navigate to "invalid takes you to /add', fakeAsync(() => {
           // comp.router.navigate(['/fghddfgh']);
            // TestBed.get(ActivatedRoute).queryParams = of({ id: 'unitTest' });
            // comp.ngOnInit();
            comp.subscribeToMemberData('unitTest');
            tick();
            expect(comp.id = '');
        }));

    // 8 //
        it('navigate to " " takes you to /add', fakeAsync(() => {
            // comp.router.navigate(['/fghddfgh']);
            // TestBed.get(ActivatedRoute).queryParams = of({ id: '' });
            // comp.ngOnInit();
            tick();
            expect(comp.id = '');
        }));

    // 9 //
        it('navigate to 999999999999 invalid user takes you to /add', fakeAsync(() => {
            // comp.router.navigate(['/fghddfgh']);
            // TestBed.get(ActivatedRoute).queryParams = of({ id: '999999999999' });
            // comp.ngOnInit();
            // comp.subscribeToMemberData('999999999999');
            tick();
            expect(comp.id = '');
        }));

    // 10 //
    it('navigate to invalid', fakeAsync(() => {

   // params.next({ 'id': 'add' });
        // fixture.detectChanges();

    // tick to make sure the async observable resolves
    tick();
        expect(comp.id).toBe('');
    }));



    afterEach(() => {
        fixture.destroy();
    });
});
