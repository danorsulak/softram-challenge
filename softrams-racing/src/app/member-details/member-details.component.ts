import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AppService} from '../app.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Member} from '../data-models/member-data';
import {NgbTooltip, NgbTooltipConfig} from '@ng-bootstrap/ng-bootstrap';

// This interface may be useful in the times ahead...
// now using data-models/member-data as member obj
/*interface Member {
  firstName: string;
  lastName: string;
  jobTitle: string;
  teamName: string;
  status: string;
}*/
const ADD_SAVE_TEXT = 'Save Member';
const UPDATE_SAVE_TEXT = 'Update Member';
const TITLE_ADD_MEMBER = 'ADD MEMBER TO RACING TEAM';
const TITLE_EDIT_MEMBER = 'EDIT MEMBER OF RACING TEAM';

@Component({
    selector: 'app-member-details',
    templateUrl: './member-details.component.html',
    styleUrls: ['./member-details.component.css']
})

export class MemberDetailsComponent implements OnInit {
    memberForm: FormGroup;
    saveText = '';
    teams = [];
    validationPassed = false;
    id: string;
    title: string;
    form: Member = {
        firstName: '',
        lastName: '',
        jobTitle: '',
        team: '',
        status: ''
    };
    // alertMessage: String;
    errorMessages: {};
    validationErrors: Member = {
        firstName: '',
        lastName: '',
        jobTitle: '',
        team: '',
        status: ''
    };

    constructor(
        private ttconfig: NgbTooltipConfig,
        private fb: FormBuilder,
        private appService: AppService,
        private router: Router,
        private activatedRoute: ActivatedRoute) {
        ttconfig.triggers = 'hover';
        this.errorMessages = {
            firstName: 'First Name is required',
            lastName: 'Last Name is required',
            jobTitle: 'Job Title is required',
            team: 'Team Name is required',
            status: 'Status is required'
        };
        // parse URL to identify if add or edit page
    }

    ngOnInit() {
        // call api to populate dropdown teams names
        this.appService.getTeams().subscribe(teams => {
            this.teams = teams;
        });
        this.memberForm = this.fb.group({
            firstName: new FormControl('', Validators.required),
            lastName: new FormControl('', Validators.required),
            jobTitle: new FormControl('', Validators.required),
            team: new FormControl('', Validators.required),
            status: new FormControl('', Validators.required)
        });
        // this.activatedRoute.url.subscribe(params => {
        this.activatedRoute.params.subscribe(params => {
            if (params) {
                console.log(params);
                let isNum = !isNaN(Number(params.id));
                console.log(isNum.toString());
                // This is an EDIT member page if parameter is a number
                if (isNum) {
                    this.subscribeToMemberData(params.id);
                    this.saveText = UPDATE_SAVE_TEXT;
                    this.id = params.id;
                    this.title = TITLE_EDIT_MEMBER;
                } else {
                    // This is a SAVE member page
                    this.id = '';
                    this.saveText = ADD_SAVE_TEXT;
                    this.title = TITLE_ADD_MEMBER;
                    // re-route all urls that get to this point to /add as we know its a save page
                    if (params.id !== 'add') {
                        this.router.navigate(['/member-details/add']);
                    }
                }
            }
        });
    }

    // TODO: Add member to members
    onSubmit() {
        // First validate form
        if (this.memberForm.valid) {
            // If no validation errors, determine if this is update or save type before submitting
                if (this.id === '') {
                    // this is a save member
                    this.appService.addMember(this.form).subscribe(result => {
                        if (result) {
                            // successful save
                            this.router.navigate(['/members']);
                        }
                    });
                } else {
                    // this is an Update member
                    this.appService.updateMember(this.form, this.id).subscribe(result => {
                        if (result) {
                            // successful update
                            this.router.navigate(['/members']);
                        }
                    });
                }
        } else {
            this.setValidationErrors();
        }
    }

    keydown(property) {
        // keydown was detected on field, lets reconsider validation status
        this.form[property] = this.form[property].trimStart();
        if (this.form[property] !== '') {
            this.validationErrors[property] = '';
        } else {
            this.validationErrors[property] = this.errorMessages[property];
        }
    }

    navigateBack() {
        this.router.navigate(['/members']);
    }

    subscribeToMemberData(id) {
        this.appService.getMember(id).subscribe(member => {
            if (member && member.firstName) {
                this.form = member;
            } else {
                this.validationPassed = false;
                this.id = '';
                this.saveText = ADD_SAVE_TEXT;
                this.form = {
                    firstName: '',
                    lastName: '',
                    jobTitle: '',
                    team: '',
                    status: ''
                };
                this.router.navigate(['/member-details/add']);
            }
        });
    }

    setValidationErrors() {
        let hasValedationErrors = false;
        for (let key of Object.keys(this.form)) {
            let formItem = this.form[key];
            if (!formItem || formItem === '') {
                // this form field did not pass validation, add to errorMessages
                this.validationErrors[key] = this.errorMessages[key];
                hasValedationErrors = true;
            } else {
                // this form field passed validation
                this.validationErrors[key] = '';
            }
        }
        if (hasValedationErrors) {
            this.validationPassed = false;
        } else {
            this.validationPassed = true;
        }
    }
}
