export class Member {
    firstName: string;
    lastName: string;
    jobTitle: string;
    team: string;
    status: string;
    id?: number | string;
}
