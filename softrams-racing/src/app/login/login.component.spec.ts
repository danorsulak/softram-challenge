import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LoginComponent} from './login.component';

import {HttpClient} from '@angular/common/http';

import {HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {Router} from '@angular/router';
import {AppService} from '../app.service';
import {fakeAsync, tick} from '../../../node_modules/@angular/core/testing';
import {Type} from '@angular/core';

describe('LoginComponent', () => {
    let comp: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [LoginComponent],
            imports: [ReactiveFormsModule, RouterModule, HttpClientModule],
            providers: [
                {
                    provide: Router,
                    useClass: class {
                        navigate = jasmine.createSpy('navigate');
                    }
                },
                HttpClient
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LoginComponent);
        comp = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(comp).toBeTruthy();
    });
    it('should login', fakeAsync(() => {
        const mockService = fixture.debugElement.injector.get<AppService>(AppService as Type<AppService>);
        comp.loginForm.value.username = 'test';
        comp.loginForm.value.password = 'test';
        tick();
        comp.login();
        tick();
        const username = localStorage.getItem('username');
        const isLoggedIn = mockService.isLoggedIn();
        expect(username && isLoggedIn).toBeTruthy();
    }));
});
