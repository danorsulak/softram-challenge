import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BannerComponent } from './banner.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import {fakeAsync, tick} from '../../../node_modules/@angular/core/testing';
import {Router} from '@angular/router';
import {AppService} from '../app.service';
import {Type} from '@angular/core';
describe('BannerComponent', () => {
  let comp: BannerComponent;
  let fixture: ComponentFixture<BannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BannerComponent ],
      imports: [ HttpClientTestingModule, RouterTestingModule ],
        providers:
        [
            {
                provide: Router,
                useClass: class {
                    navigate = jasmine.createSpy('navigate');
                }
            }
        ],
  })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BannerComponent);
    comp = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(comp).toBeTruthy();
  });
    it('logout', fakeAsync(() => {
        const mockService = fixture.debugElement.injector.get<AppService>(AppService as Type<AppService>);
        localStorage.setItem('username', 'test');
        comp.appService.setUsername('test');
        tick();
        comp.logout();
        tick();
        let isLoggedIn = mockService.isLoggedIn();
        const username = localStorage.getItem('username');
        expect(username === '' && !isLoggedIn);
    }));
});
