import { TestBed, inject } from '@angular/core/testing';

import { AppService } from './app.service';

import { HttpClientModule } from '@angular/common/http';
import {MembersComponent} from './members/members.component';


describe('AppService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
        providers: [AppService],
        imports: [HttpClientModule]
    });
  });
    beforeEach(() => {
        spyOn(window, 'confirm').and.returnValue(true);
    });
  it('should be created', inject([AppService], (service: AppService) => {
    expect(service).toBeTruthy();
  }));
});
